var selectorOptions = {
    buttons: [{
        step: 'day',
        stepmode: 'backward',
        count: 1,
        label: '1d'
    }, {
        step: 'day',
        stepmode: 'backward',
        count: 5,
        label: '5d'
    }, {
        step: 'all',
    }],
};

$( function() {
    $( "#city" ).change(
        function( ) {
            var cityCode = $(this).val();
            var cityName = $(this).find(":selected").text();
            draw(cityCode, cityName);
        }
    );



  } );


function draw(cityId, cityName) {
Plotly.d3.json('/forecast/' + cityId, function(err, rawData) {
    if(err) throw err;

    var data = prepData(rawData);
    var layout = {
        title: '5 day forecast for ' + cityName,
        showlegend: false,
        xaxis: {
            rangeselector: selectorOptions,
            rangeslider: {}
        },
        yaxis: {
    title: 'Temperature (&deg;C)',
    titlefont: {color: '#1f77b4'},
    tickfont: {color: '#1f77b4'},
    domain: [0, 0.30]
  },
  yaxis2: {
    title: 'Pressure (hPa)',
    titlefont: {color: '#ff7f0e'},
    tickfont: {color: '#ff7f0e'},
    domain: [0.33, 0.66]

  },
  yaxis3: {
    title: 'Humidity (%)',
    titlefont: {color: '#d62728'},
    tickfont: {color: '#d62728'},
    domain: [0.7, 1]

  },
        width: 800,
        height: 800
    };

    Plotly.newPlot('graph', data, layout);
})
};


function prepData(rawData) {
    var x = [];
    var temps = [];
    var pressure = [];
    var humidity = [];

    rawData.weatherPoints.forEach(function(datum, i) {

        x.push(new Date(datum.date));
        temps.push(datum.temperature -273.15);
        pressure.push(datum.pressure);
        humidity.push(datum.humidity);
    });

    return [
    {
        mode: 'scatter',
        x: x,
        y: temps,
        yaxis: 'y1',
        name: 'Temp'
    }, {
        mode: 'scatter',
        x: x,
        y: pressure,
        yaxis: 'y2',
        name: 'Pressure'
    }, {
        mode: 'scatter',
        x: x,
        y: humidity,
        yaxis: 'y3',
        name: 'Humidity',
        line: {
            shape: 'scatter' ,
            color: 'red'
        }
    }];
}
