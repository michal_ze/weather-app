package com.example.weather.rest;

import com.example.weather.client.WeatherProvider;
import com.example.weather.dto.Forecast;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ForecastService {

    @Autowired
    private WeatherProvider weatherProvider;

    @GetMapping(value = "/forecast/{city}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Forecast getForecast(@PathVariable("city") String city) {
        return weatherProvider.getForecast(city);
    }
}
