package com.example.weather.client;

import com.example.weather.dto.Forecast;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
public class WeatherProvider {

    @Autowired
    private OpenWeatherClient client;

    @Cacheable("forecast")
    public Forecast getForecast(String cityCode) {
        return client.getForecast(cityCode);
    }
}
