package com.example.weather.client;

import com.example.weather.dto.Forecast;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class OpenWeatherClient {

    private static final String PATH = "/data/2.5/forecast?id={cityId}&appid={appKey}";

    private final String apiKey;

    private final RestTemplate template;

    public OpenWeatherClient(RestTemplateBuilder builder, @Value("${api.url}") String serviceURL,  @Value("${api.key}") String apiKey) {
        this.template = builder.rootUri(serviceURL).build();
        this.apiKey = apiKey;
    }


    public Forecast getForecast(String city) {
        return template.getForObject(PATH, Forecast.class, city, apiKey);
    }
}
