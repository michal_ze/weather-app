package com.example.weather.dto;

public class WeatherPoint {

    private final String temperature;
    private final String humidity;
    private final String pressure;
    private final String date;

    public WeatherPoint(String temperature, String humidity, String pressure, String date) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        this.date = date;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getPressure() {
        return pressure;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "WeatherPoint{" +
                "temperature='" + temperature + '\'' +
                ", humidity='" + humidity + '\'' +
                ", pressure='" + pressure + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
