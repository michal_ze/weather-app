package com.example.weather.dto;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ForecastDeserializer extends JsonDeserializer<Forecast> {
    @Override
    public Forecast deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {

        JsonNode productNode = jsonParser.getCodec().readTree(jsonParser);
        JsonNode listNode = productNode.get("list");

        Iterator<JsonNode> elements = listNode.elements();

        List<WeatherPoint> points = new ArrayList<>();
        while (elements.hasNext()) {
            JsonNode node = elements.next();
            String dateTxt = node.get("dt_txt").asText();
            JsonNode mainNode = node.get("main");
            points.add(new WeatherPoint(
                    mainNode.get("temp").asText(),
                    mainNode.get("humidity").asText(),
                    mainNode.get("pressure").asText(),
                    dateTxt));
        }


        return new Forecast(points);
    }
}
