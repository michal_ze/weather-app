package com.example.weather.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

@JsonDeserialize(using = ForecastDeserializer.class)
public class Forecast {

    private final List<WeatherPoint> weatherPoints;

    public Forecast(List<WeatherPoint> weatherPoints) {
        this.weatherPoints = weatherPoints;
    }

    public List<WeatherPoint> getWeatherPoints() {
        return weatherPoints;
    }

    @Override
    public String toString() {
        return "Forecast{" +
                "weatherPoints=" + weatherPoints +
                '}';
    }
}
