package com.example.weather.rest;

import com.example.weather.client.WeatherProvider;
import com.example.weather.dto.Forecast;
import com.example.weather.dto.WeatherPoint;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ForecastService.class)
public class ForecastServiceWebTest {

    @MockBean
    WeatherProvider weatherProvider;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getForecast_GET() throws Exception {
        // given
        final String cityCode = "123";
        when(weatherProvider.getForecast(cityCode)).thenReturn(mockForecast());

        mockMvc.perform(
                // when
                get("/forecast/{city}", cityCode).accept(MediaType.APPLICATION_JSON))
                // then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.weatherPoints", hasSize(1)))
                .andExpect(jsonPath("$.weatherPoints[0].temperature", equalTo("1")))
                .andExpect(jsonPath("$.weatherPoints[0].date", equalTo("today")));
    }


    private static Forecast mockForecast() {
        WeatherPoint p = new WeatherPoint("1", "2", "3", "today");
        return new Forecast(Collections.singletonList(p));
    }
}