package com.example.weather.dto;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.InputStream;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class ForecastDeserializerTest {

    private ObjectMapper mapper = new ObjectMapper();

    private ForecastDeserializer deserializer = new ForecastDeserializer();

    @Test
    public void testDeserialize() throws Exception {
        // given
        InputStream stream = new ClassPathResource("test_forecast_small.json").getInputStream();
        JsonParser parser = mapper.getFactory().createParser(stream);
        DeserializationContext ctx = mapper.getDeserializationContext();

        // when
        Forecast forecast = deserializer.deserialize(parser, ctx);

        // then
        assertThat(forecast).isNotNull();
        assertThat(forecast.getWeatherPoints()).hasSize(1);

        WeatherPoint point = forecast.getWeatherPoints().get(0);
        assertThat(point.getTemperature()).isEqualTo("286.67");
        assertThat(point.getHumidity()).isEqualTo("75");
        assertThat(point.getPressure()).isEqualTo("972.73");
        assertThat(point.getDate()).isEqualTo("2017-02-16 12:00:00");
    }
}