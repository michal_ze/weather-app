package com.example.weather.client;

import com.example.weather.dto.Forecast;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.queryParam;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@RestClientTest(OpenWeatherClient.class)
public class OpenWeatherClientTest {

    @Autowired
    OpenWeatherClient client;

    @Autowired
    MockRestServiceServer server;

    @Value("${api.key}")
    String apiKey;

    @Test
    public void testGetForecast() throws Exception {
        // given
        final String cityId = "123";
        server.expect(method(GET))
                .andExpect(queryParam("id", cityId))
                .andExpect(queryParam("appid", apiKey))
                .andRespond(withSuccess(new ClassPathResource("test_forecast.json"), MediaType.APPLICATION_JSON));

        // when
        Forecast forecast = client.getForecast(cityId);

        // then
        assertThat(forecast).isNotNull();
        assertThat(forecast.getWeatherPoints()).isNotEmpty();
        assertThat(forecast.getWeatherPoints().get(0).getTemperature()).isEqualTo("286.67");
    }

}